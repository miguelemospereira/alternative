package switch2019.project.controllerLayer.controllers.controllersREST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import switch2019.project.applicationLayer.applicationServices.US004GroupsThatAreFamilyService;
import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtos.GroupMembersDTO;
import switch2019.project.applicationLayer.dtos.GroupsThatAreFamilyDTO;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * ControllerRESTUS004
 */
//US004. Como gestor de sistema, quero saber quais os grupos que são família.

@RestController
public class US004GroupsThatAreFamilyControllerREST {

    @Autowired
    private US004GroupsThatAreFamilyService serviceUS004;

    /**
     * Groups that are family.
     *
     * @return the response entity
     */

    @GetMapping("/groups/areFamily")
    public ResponseEntity<Object> getGroupsThatAreFamily() {

        GroupsThatAreFamilyDTO result = serviceUS004.groupsThatAreFamily();

        for(int i = 0; i < result.getGroupThatAreFamily().size(); i++) {
            String denomination = result.getGroupThatAreFamily().get(i).getDenomination();
            Link self_link = linkTo(methodOn(US004GroupsThatAreFamilyControllerREST.class).getGroupByDenomination(denomination)).withSelfRel();
            result.add(self_link);
        }

        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @GetMapping("/groups/areFamily/{groupDenomination}")
    public ResponseEntity<Object> getGroupByDenomination(@PathVariable final String groupDenomination) {

        GroupDTO result = serviceUS004.getGroupByDenomination(groupDenomination);

        Link link_to_allMembers = linkTo(methodOn(US004GroupsThatAreFamilyControllerREST.class).getGroupAllMembers(groupDenomination)).withRel("allMembers");

        result.add(link_to_allMembers);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/groups/areFamily/{groupDenomination}/allMembers")
    public ResponseEntity<Object> getGroupAllMembers(@PathVariable final String groupDenomination) {

        GroupMembersDTO result = serviceUS004.getGroupAllMembers(groupDenomination);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}