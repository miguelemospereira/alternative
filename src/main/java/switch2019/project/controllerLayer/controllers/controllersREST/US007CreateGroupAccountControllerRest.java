package switch2019.project.controllerLayer.controllers.controllersREST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import switch2019.project.applicationLayer.applicationServices.US007CreateGroupAccountService;
import switch2019.project.applicationLayer.dtos.CreateGroupAccountDTO;
import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtos.NewGroupAccountInfoDTO;
import switch2019.project.applicationLayer.dtosAssemblers.CreateGroupAccountDTOAssembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/* US007 Como responsável do grupo, quero criar uma conta do grupo,
   atribuindo-lhe uma denominação e uma descrição, para posteriormente poder ser usada nos movimentos do grupo.
*/

@RestController
public class US007CreateGroupAccountControllerRest {

    @Autowired
    private US007CreateGroupAccountService service;

    @PostMapping("/persons/{personEmail}/groups/{groupDenomination}/accounts")
    public ResponseEntity<Object> createGroupAccount(@RequestBody NewGroupAccountInfoDTO info,
                                                     @PathVariable final String personEmail,
                                                     @PathVariable final String groupDenomination) {

        CreateGroupAccountDTO createGroupAccountDTO = CreateGroupAccountDTOAssembler.createDTOFromPrimitiveTypes(
                personEmail, groupDenomination, info.getAccountDescription(), info.getAccountDenomination());

        GroupDTO result = service.createAccountAsPeopleInCharge(createGroupAccountDTO);

        Link link_to_admins = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAdmins(groupDenomination)).withRel("admins");
        Link link_to_members = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupMembers(groupDenomination)).withRel("members");
        Link link_to_ledger = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupLedger(groupDenomination)).withRel("ledger");
        Link link_to_accounts = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAccounts(personEmail, groupDenomination)).withRel("accounts");
        Link link_to_categories = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupCategories(personEmail,groupDenomination)).withRel("categories");

        result.add(link_to_admins);
        result.add(link_to_members);
        result.add(link_to_ledger);
        result.add(link_to_accounts);
        result.add(link_to_categories);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }
}
