package switch2019.project.controllerLayer.controllers.controllersCLI;

import switch2019.project.applicationLayer.applicationServices.US008_1CreateGroupTransactionService;
import switch2019.project.applicationLayer.dtos.BooleanDTO;
import switch2019.project.applicationLayer.dtos.CreateGroupTransactionDTO;
import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtosAssemblers.CreateGroupTransactionDTOAssembler;

/**
 * The type Us 008 1 create group transaction controller.
 *
 * @author SWitCH 2019/2020 Group 3
 * @author Joana Correia
 * @version %I%, %G% <p> Controller  for US 8.1, where a member of a group wants to create a group transaction. The transaction shall have: - an amount - a date (by default, current date) - a description - a category - a debit account - a credit account
 */
public class US008_1CreateGroupTransactionController {
    private US008_1CreateGroupTransactionService us008_1CreateGroupTransactionService;

    /**
     * Instantiates a new Us 008 1 create group transaction controller.
     *
     * @param us008_1CreateGroupTransactionService the us 008 1 create group transaction service
     */
    public US008_1CreateGroupTransactionController(US008_1CreateGroupTransactionService us008_1CreateGroupTransactionService) {
        this.us008_1CreateGroupTransactionService = us008_1CreateGroupTransactionService;
    }

    /**
     * Create group transaction boolean dto.
     *
     * @param groupDenomination      Name of the group where the transaction refers to
     * @param personGroupMemberEmail Email of the member of the group that wants to add the transaction
     * @param categoryDenomination   Category that classifies the transaction
     * @param accountToDebitName     Denomination of the account where amount will be debited (subtracted)
     * @param accountToCreditName    Denomination of the account where amount will be credited (added)
     * @param transactionAmount      Amount of money transferred from account to debit and account to credit
     * @param transactionType        Type of transaction (credit, between accountToDebit to accountToCredit; debit between accountToCredit to accountToDebit)
     * @param transactionDescription Description related with the scope of the transaction
     * @return us008_1CreateGroupTransactionService.createGroupTransaction(createGroupTransactionDTO) boolean dto
     */
    public GroupDTO createGroupTransaction(String groupDenomination, String personGroupMemberEmail, String categoryDenomination, String accountToDebitName, String accountToCreditName, double transactionAmount, String transactionType, String transactionDescription, String date) {
        CreateGroupTransactionDTO createGroupTransactionDTO = CreateGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(groupDenomination, personGroupMemberEmail, categoryDenomination, accountToDebitName, accountToCreditName, transactionAmount, transactionType, transactionDescription, date);
        return us008_1CreateGroupTransactionService.createGroupTransaction(createGroupTransactionDTO);
    }
}
