package switch2019.project.controllerLayer.controllers.controllersCLI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import switch2019.project.applicationLayer.applicationServices.US010GroupSearchAccountRecordsService;
import switch2019.project.applicationLayer.dtos.GroupSearchAccountRecordsInDTO;
import switch2019.project.applicationLayer.dtos.SearchAccountRecordsOutDTO;
import switch2019.project.applicationLayer.dtosAssemblers.GroupSearchAccountRecordsInDTOAssembler;

/**
 * US010 - As a group member, I want to obtain the transactions of the group, for a given account,
 * within a given period
 */
public class US010GroupSearchAccountRecordsController {

    private US010GroupSearchAccountRecordsService us010GroupSearchAccountRecordsService;

    /**
     * Instantiates a new controller, for getting the group transactions, by a member,
     * for a given account, within a given period.
     *
     * @param us010GroupSearchAccountRecordsService the group search account records service
     */
    public US010GroupSearchAccountRecordsController(US010GroupSearchAccountRecordsService us010GroupSearchAccountRecordsService) {
        this.us010GroupSearchAccountRecordsService = us010GroupSearchAccountRecordsService;
    }

    /**
     * Gets group account transactions within a period.
     *
     * @param personEmail         the person email (group member)
     * @param groupDenomination   the group denomination
     * @param accountDenomination the denomination of the account to search
     * @param startDate           the start date of the period to search
     * @param endDate             the end date of the period to search
     * @return the group account transactions within a period
     */
    public SearchAccountRecordsOutDTO getGroupAccountTransactionsWithinPeriod(String personEmail, String groupDenomination, String accountDenomination, String startDate, String endDate) {
        GroupSearchAccountRecordsInDTO groupSearchAccountRecordsInDTO = GroupSearchAccountRecordsInDTOAssembler.groupSearchAccountRecordsInDTO(personEmail, groupDenomination, accountDenomination, startDate, endDate);
        return us010GroupSearchAccountRecordsService.getGroupAccountTransactionsWithinPeriod(groupSearchAccountRecordsInDTO);
    }
}
