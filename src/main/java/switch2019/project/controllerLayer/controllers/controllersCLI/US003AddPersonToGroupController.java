package switch2019.project.controllerLayer.controllers.controllersCLI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import switch2019.project.applicationLayer.applicationServices.US003AddPersonToGroupService;
import switch2019.project.applicationLayer.dtos.AddPersonToGroupDTO;
import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtosAssemblers.AddPersonToGroupDTOAssembler;

/**
 * The type Us 003 add person to group controller.
 *
 * @author Elisabete_Cavaleiro
 */
//US03. Como gestor de sistema, quero acrescentar pessoas ao grupo.

@Controller
public class US003AddPersonToGroupController {


    @Autowired
    private final US003AddPersonToGroupService us003AddPersonToGroupService;

    /**
     * Instantiates a new Us 003 add person to group controller.
     *
     * @param us003AddPersonToGroupService the us 003 add person to group service
     */
    public US003AddPersonToGroupController(US003AddPersonToGroupService us003AddPersonToGroupService) {
        this.us003AddPersonToGroupService = us003AddPersonToGroupService;
    }

    /**
     * Add person to group boolean dto.
     *
     * @param email        the email
     * @param denomination the denomination
     * @return the boolean dto
     */
    public GroupDTO addPersonToGroup(String email, String denomination){
        AddPersonToGroupDTO addPersonToGroupDTO = AddPersonToGroupDTOAssembler.createDataTransferObject_Primitives(email, denomination);
        return us003AddPersonToGroupService.addPersonToGroup(addPersonToGroupDTO);

    }
}
