package switch2019.project.controllerLayer.controllers.controllersCLI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import switch2019.project.applicationLayer.applicationServices.US006CreatePersonAccountService;
import switch2019.project.applicationLayer.dtos.CreatePersonAccountDTO;
import switch2019.project.applicationLayer.dtos.PersonDTO;
import switch2019.project.applicationLayer.dtosAssemblers.CreatePersonAccountDTOAssembler;

    /*
    US 06. Como utilizador, quero criar uma conta para mim, atribuindo-lhe uma
    denominação e uma descrição, para posteriormente poder ser usada nos meus movimentos.
     */

/**
 * The type Us 006 create person account controller.
 */
@Controller
public class US006CreatePersonAccountController {

    @Autowired
    private US006CreatePersonAccountService us006CreatePersonAccountService;

    /**
     * Instantiates a new Us 006 create person account controller.
     *
     * @param us006CreatePersonAccountService the us 006 create person account service
     */

    public US006CreatePersonAccountController(US006CreatePersonAccountService us006CreatePersonAccountService) {
        this.us006CreatePersonAccountService = us006CreatePersonAccountService;

    }

    /**
     * Create account boolean dto.
     *
     * @param email        the email
     * @param description  the account description
     * @param denomination the account denomination
     * @return the boolean dto
     */

    public PersonDTO createAccount (String email, String description, String denomination) {
        CreatePersonAccountDTO createPersonAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(email, description, denomination);
        return us006CreatePersonAccountService.createAccount(createPersonAccountDTO);
    }


}
