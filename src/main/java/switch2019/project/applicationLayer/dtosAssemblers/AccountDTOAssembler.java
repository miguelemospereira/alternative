package switch2019.project.applicationLayer.dtosAssemblers;

import switch2019.project.applicationLayer.dtos.AccountDTO;

public class AccountDTOAssembler {

    public static AccountDTO createDTOFromPrimitiveTypes(String denomination, String description) {

        AccountDTO accountsDTO = new AccountDTO(denomination, description);

        return accountsDTO;
    }
}
