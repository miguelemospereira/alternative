package switch2019.project.applicationLayer.dtosAssemblers;

import switch2019.project.applicationLayer.dtos.PersonEmailDTO;

/**
 * @author Ala Matos
 */

public class PersonEmailDTOAssembler {

    /**
     * Create dto from primitive types boolean dto.
     *
     * @param email
     * @return the PersonEmailDTO
     */

    public static PersonEmailDTO createPersonEmailDTO(String email) {
        return new PersonEmailDTO(email);
    }
}
