package switch2019.project.applicationLayer.dtosAssemblers;

import switch2019.project.applicationLayer.dtos.GroupMemberClearanceDTO;

public class GroupMemberClearanceDTOAssembler {

    public static GroupMemberClearanceDTO createDTOFromDomainObject(String memberID, String clearance) {

        GroupMemberClearanceDTO groupMemberClearanceDTO = new GroupMemberClearanceDTO(memberID, clearance);
        return groupMemberClearanceDTO;
    }

}
