package switch2019.project.applicationLayer.applicationServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switch2019.project.applicationLayer.dtos.CreatePersonCategoryDTO;
import switch2019.project.applicationLayer.dtos.PersonDTO;
import switch2019.project.applicationLayer.dtosAssemblers.PersonDTOAssembler;
import switch2019.project.domainLayer.domainEntities.aggregates.category.Category;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Person;
import switch2019.project.domainLayer.domainEntities.vosShared.CategoryID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.ICategoryRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.IPersonRepository;

import java.util.Optional;

/**
 * The type Us 005 create person category service.
 */
@Service
public class US005CreatePersonCategoryService {

    @Autowired
    private IPersonRepository personRepository;
    @Autowired
    private ICategoryRepository categoryRepository;

    /**
     * Instantiates a new Us 005 create person category service.
     *
     * @param personRepository   the person repository
     * @param categoryRepository the category repository
     */
    public US005CreatePersonCategoryService(IPersonRepository personRepository, ICategoryRepository categoryRepository) {
        this.personRepository = personRepository;
        this.categoryRepository = categoryRepository;
    }

    /**
     * The constant SUCCESS.
     */
//Return messages
    public final static String SUCCESS = "Category created and added";
    /**
     * The constant CATEGORY_ALREADY_EXIST.
     */
    public final static String CATEGORY_ALREADY_EXIST = "Category already exists";
    /**
     * The constant PERSON_DOES_NOT_EXIST.
     */
    public final static String PERSON_DOES_NOT_EXIST = "Person does not exist";

    /**
     * Create category as people in charge boolean dto.
     *
     * @param createPersonCategoryDTO the create group category dto
     * @return the boolean dto
     */


    public PersonDTO createCategory(CreatePersonCategoryDTO createPersonCategoryDTO) {
        Person person;

        PersonID personID = PersonID.createPersonID(createPersonCategoryDTO.getEmail());
        Optional<Person> optPerson = personRepository.findById(personID);

        if (!optPerson.isPresent()) {

            throw new NotFoundArgumentsBusinessException(PERSON_DOES_NOT_EXIST);

        } else {
            person = optPerson.get();

            CategoryID categoryID = CategoryID.createCategoryID(createPersonCategoryDTO.getDenomination(), personID);
            boolean categoryExists = categoryRepository.existsById(categoryID);

            if (categoryExists) {

                throw new InvalidArgumentsBusinessException(CATEGORY_ALREADY_EXIST);

            } else {
                person.addCategory(categoryID);
                personRepository.addAndSaveCategory(person);

            }
        }
        return PersonDTOAssembler.createDTOFromDomainObject(
                person.getPersonID().getEmail(), person.getName(),
                person.getBirthdate(), person.getBirthplace(),
                person.getFather(), person.getMother());
    }
}
