package switch2019.project.applicationLayer.applicationServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import switch2019.project.applicationLayer.dtos.AddPersonToGroupDTO;
import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtosAssemblers.GroupDTOAssembler;
import switch2019.project.domainLayer.domainEntities.aggregates.group.Group;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Person;
import switch2019.project.domainLayer.domainEntities.vosShared.GroupID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.IGroupRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.IPersonRepository;

import java.util.Optional;

/**
 * The type Us 003 add person to group service.
 *
 * @author Elisabete_Cavaleiro
 */
//US03. As a system manager I want to add person to group

@Service
public class US003AddPersonToGroupService {

    @Autowired
    private final IPersonRepository personRepository;

    @Autowired
    private final IGroupRepository groupRepository;

    /**
     * The constant SUCCESS.
     */
//Return messages
    /**
     * The constant PERSON_DOES_NOT_EXIST.
     */
    public static final String PERSON_DOES_NOT_EXIST = "Person does not exist";
    /**
     * The constant GROUP_DOES_NOT_EXIST.
     */
    public static final String GROUP_DOES_NOT_EXIST = "Group does not exist";
    /**
     * The constant PERSON_ALREADY_EXIST_IN_THE_GROUP.
     */
    public static final String PERSON_ALREADY_EXIST_IN_THE_GROUP = "Person is already member";

    /**
     * Instantiates a new Us 003 add person to group service.
     *
     * @param personRepository the person repository
     * @param groupRepository  the group repository
     */
    public US003AddPersonToGroupService(IPersonRepository personRepository, IGroupRepository groupRepository) {
        this.personRepository = personRepository;
        this.groupRepository = groupRepository;
    }


    /**
     * Add person to group boolean dto.
     *
     * @param addPersonToGroupDTO the add person to group dto
     * @return the boolean dto
     */
    @Transactional
    public GroupDTO addPersonToGroup(AddPersonToGroupDTO addPersonToGroupDTO) {

        Group group;

        PersonID memberID = PersonID.createPersonID(addPersonToGroupDTO.getEmail());

        boolean personExist = personRepository.exists(memberID);

        if (!personExist) {

            throw new InvalidArgumentsBusinessException(PERSON_DOES_NOT_EXIST);

        } else {
            GroupID groupID = GroupID.createGroupID(addPersonToGroupDTO.getDenomination());

            Optional<Group> opGroup = groupRepository.findById(groupID);

            if (!opGroup.isPresent() ) {

                throw new NotFoundArgumentsBusinessException(GROUP_DOES_NOT_EXIST);

            } else {
                group = opGroup.get();

                boolean isPersonAMember = group.isPersonAlreadyMember(memberID);

                if (isPersonAMember) {
                    throw new InvalidArgumentsBusinessException(PERSON_ALREADY_EXIST_IN_THE_GROUP);

                } else {
                    group.addMember(memberID);
                    groupRepository.addAndSaveMember(group, memberID);
                }

            }
            return GroupDTOAssembler.createDTOFromDomainObject(group.getGroupID().getDenomination(), group.getDescription(), group.getDateOfCreation());
        }

    }
}