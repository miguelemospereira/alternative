package switch2019.project.controllerLayer.unitTests.controllersCLI;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import switch2019.project.applicationLayer.applicationServices.US012GroupTransactionsWithinPeriodService;
import switch2019.project.applicationLayer.dtos.GroupTransactionsWithinPeriodDTOin;
import switch2019.project.applicationLayer.dtos.GroupTransactionsWithinPeriodDTOout;
import switch2019.project.applicationLayer.dtosAssemblers.GroupTransactionsWithinPeriodDTOinAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.GroupTransactionsWithinPeriodDTOoutAssembler;
import switch2019.project.controllerLayer.controllers.controllersCLI.US012GroupTransactionsWithinPeriodController;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Transaction;
import switch2019.project.domainLayer.domainEntities.vosShared.*;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class US012GetGroupTransactionsWithinAPeriodControllerTest extends AbstractTest {

    @Mock
    private US012GroupTransactionsWithinPeriodService us012Service;


    @Test
    @DisplayName("Test for service method getGroupTransactionsWithinPeriod() - Success")
    void getGroupTransactionsWithinPeriod_Success() {

        //ARRANGE

        //Dates to search
        LocalDate startDate = LocalDate.of(2020, 01, 15);
        LocalDate endDate = LocalDate.of(2020, 03, 15);

        //Arrange person info - group member
        String personGroupMemberEmail = "paulo@gmail.com";

        //Arrange group info
        String groupDenomination = "House";
        GroupID houseID = GroupID.createGroupID(groupDenomination);

        //Arrange category
        String denominationExpenses = "Electricity Expenses";
        CategoryID categoryExpensesID = CategoryID.createCategoryID(denominationExpenses, houseID);

        //Arrange accounts
        //Account House Wallet
        String houseWalletAccountDenomination = "House Funds";
        AccountID houseWalletAccountID = AccountID.createAccountID(houseWalletAccountDenomination, houseID);

        //Account EDP
        String houseEdpAccountDenomination = "EDP";
        AccountID houseEdpID = AccountID.createAccountID(houseEdpAccountDenomination, houseID);

        //Arrange transactions
        //Transaction 1 on Group House
        String typeTransaction1House = "Debit";
        String descriptionTransaction1House = "EDP bill from January/2020";
        LocalDate dateTransaction1House = LocalDate.of(2020, 02, 05);
        double amountTransaction1House = 40.00;
        Transaction houseTransaction1 = Transaction.createTransaction(categoryExpensesID, typeTransaction1House, descriptionTransaction1House, amountTransaction1House, dateTransaction1House, houseWalletAccountID, houseEdpID);

        //Transaction 2 on Group House
        String typeTransaction2House = "Debit";
        String descriptionTransaction2House = "EDP bill from February/2020";
        LocalDate dateTransaction2House = LocalDate.of(2020, 03, 05);
        double amountTransaction2House = 40.00;
        Transaction houseTransaction2 = Transaction.createTransaction(categoryExpensesID, typeTransaction2House, descriptionTransaction2House, amountTransaction2House, dateTransaction2House, houseWalletAccountID, houseEdpID);

        //Arrange DTO in
        GroupTransactionsWithinPeriodDTOin groupTransactionsWithinPeriodDTOin = GroupTransactionsWithinPeriodDTOinAssembler.createGroupTransactionsWithinPeriodDTOin(personGroupMemberEmail, groupDenomination, startDate, endDate);

        //Expected DTO out
        ArrayList<Transaction> expectedTransactions = new ArrayList<>();
        expectedTransactions.add(houseTransaction1);
        expectedTransactions.add(houseTransaction2);

        GroupTransactionsWithinPeriodDTOout expectedGroupTransactionsWithinPeriodDTOout = GroupTransactionsWithinPeriodDTOoutAssembler.getGroupTransactionsWithinPeriodDTOout(expectedTransactions);

        //Mock the behaviour of the service method getGroupTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us012Service.getGroupTransactionsWithinPeriod(groupTransactionsWithinPeriodDTOin)).thenReturn(expectedGroupTransactionsWithinPeriodDTOout);


        //ACT
        US012GroupTransactionsWithinPeriodController us012Controller = new US012GroupTransactionsWithinPeriodController(us012Service);
        GroupTransactionsWithinPeriodDTOout result = us012Controller.getGroupTransactionsWithinPeriodDTOout(personGroupMemberEmail, groupDenomination, startDate, endDate);

        //ASSERT
        assertEquals(expectedGroupTransactionsWithinPeriodDTOout, result);
    }


    @Test
    @DisplayName("Test for service method getGroupTransactionsWithinPeriod() - Fail (group does not exist)")
    void getGroupTransactionsWithinPeriod_Fail_GroupDoesNotExist() {

        //ARRANGE

        //Dates to search
        LocalDate startDate = LocalDate.of(2020, 01, 15);
        LocalDate endDate = LocalDate.of(2020, 03, 15);

        //Arrange person info - group member
        String personGroupMemberEmail = "paulo@gmail.com";

        //Arrange group info
        String groupDenomination = "Swimming team";

        //Arrange DTO in
        GroupTransactionsWithinPeriodDTOin groupTransactionsWithinPeriodDTOin = GroupTransactionsWithinPeriodDTOinAssembler.createGroupTransactionsWithinPeriodDTOin(personGroupMemberEmail, groupDenomination, startDate, endDate);

        //Mock the behaviour of the service method getGroupTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us012Service.getGroupTransactionsWithinPeriod(groupTransactionsWithinPeriodDTOin)).thenThrow(new NotFoundArgumentsBusinessException(US012GroupTransactionsWithinPeriodService.GROUP_DOES_NOT_EXIST));

        //Expected message
        String expectedMessage = "Group does not exist in the system";

        //ACT
        US012GroupTransactionsWithinPeriodController us012Controller = new US012GroupTransactionsWithinPeriodController(us012Service);
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us012Controller.getGroupTransactionsWithinPeriodDTOout(personGroupMemberEmail, groupDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for service method getGroupTransactionsWithinPeriod() - Fail (person is not a member)")
    void getGroupTransactionsWithinPeriod_Fail_PersonIsNotMember() {

        //ARRANGE

        //Dates to search
        LocalDate startDate = LocalDate.of(2020, 01, 15);
        LocalDate endDate = LocalDate.of(2020, 03, 15);

        //Arrange person info - group member
        String personGroupMemberEmail = "isolina@gmail.com";

        //Arrange group info
        String groupDenomination = "House";

        //Arrange DTO in
        GroupTransactionsWithinPeriodDTOin groupTransactionsWithinPeriodDTOin = GroupTransactionsWithinPeriodDTOinAssembler.createGroupTransactionsWithinPeriodDTOin(personGroupMemberEmail, groupDenomination, startDate, endDate);

        //Mock the behaviour of the service method getGroupTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us012Service.getGroupTransactionsWithinPeriod(groupTransactionsWithinPeriodDTOin)).thenThrow(new InvalidArgumentsBusinessException(US012GroupTransactionsWithinPeriodService.PERSON_NOT_MEMBER));

        //Expected message
        String expectedMessage = "Person is not member of the group";

        //ACT
        US012GroupTransactionsWithinPeriodController us012Controller = new US012GroupTransactionsWithinPeriodController(us012Service);
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us012Controller.getGroupTransactionsWithinPeriodDTOout(personGroupMemberEmail, groupDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }
}