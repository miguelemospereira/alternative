package switch2019.project.controllerLayer.unitTests.controllersCLI;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import switch2019.project.applicationLayer.applicationServices.US003AddPersonToGroupService;
import switch2019.project.applicationLayer.dtos.AddPersonToGroupDTO;
import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtosAssemblers.AddPersonToGroupDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.GroupDTOAssembler;
import switch2019.project.controllerLayer.controllers.controllersCLI.US003AddPersonToGroupController;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.vosShared.DateOfCreation;
import switch2019.project.domainLayer.domainEntities.vosShared.Denomination;
import switch2019.project.domainLayer.domainEntities.vosShared.Description;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @Elisabete_Cavaleiro
 */

public class US003AddPersonToGroupControllerTest extends AbstractTest {


    @Mock
    private US003AddPersonToGroupService us003AddPersonToGroupService;


    @Test
    @DisplayName("Test Controller_US03 -HappyCase - Person Exist, Group Exist, Person is not already a Member")
    void addPersonToGroup_Success()  {

        //Arrange


        String email = "alexandre@gmail.com";
        String gDenomination = "Sunday Runners";
        String gDescription = "All members from Sunday Runners group";


        //Arrange expected result

        //Expected result
        Denomination denomination = Denomination.createDenomination(gDenomination);
        Description description = Description.createDescription(gDescription);
        DateOfCreation dateOfCreation = DateOfCreation.createDateOfCreation(LocalDate.now());

        GroupDTO personIsAddedToGroup= GroupDTOAssembler.createDTOFromDomainObject(denomination, description, dateOfCreation);

        AddPersonToGroupDTO addPersonToGroupDTO = AddPersonToGroupDTOAssembler.createDataTransferObject_Primitives(email, gDenomination);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)

        Mockito.when(us003AddPersonToGroupService.addPersonToGroup(addPersonToGroupDTO)).thenReturn(personIsAddedToGroup);

        //Act
        US003AddPersonToGroupController us003AddPersonToGroupController = new US003AddPersonToGroupController(us003AddPersonToGroupService);
        GroupDTO result = us003AddPersonToGroupController.addPersonToGroup(email, gDenomination);

        //Assert

        assertEquals(personIsAddedToGroup, result);

    }


    @Test
    @DisplayName("Test Controller_US03 -SadCase - Person Exist, Group Exist, Person is already a Member")
    void controller_US03_false_PersonAlreadyAMember() {

        //Arrange


        String email = "paulo@gmail.com";
        String gDenomination = "Sunday Runners";
        String gDescription = "All members from Sunday Runners group";


        //Arrange expected result

        //Expected result
        Denomination denomination = Denomination.createDenomination(gDenomination);
        Description description = Description.createDescription(gDescription);
        DateOfCreation dateOfCreation = DateOfCreation.createDateOfCreation(LocalDate.now());

        GroupDTO personIsAddedToGroup= GroupDTOAssembler.createDTOFromDomainObject(denomination, description, dateOfCreation);

        AddPersonToGroupDTO addPersonToGroupDTO = AddPersonToGroupDTOAssembler.createDataTransferObject_Primitives(email, gDenomination);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)

        Mockito.when(us003AddPersonToGroupService.addPersonToGroup(addPersonToGroupDTO)).thenReturn(personIsAddedToGroup);

        //Act
        US003AddPersonToGroupController us003AddPersonToGroupController = new US003AddPersonToGroupController(us003AddPersonToGroupService);
        GroupDTO result = us003AddPersonToGroupController.addPersonToGroup(email, gDenomination);

        //Assert

        assertEquals(personIsAddedToGroup, result);

    }

    @Test
    @DisplayName("Test Controller_US03- PersonID doesn't exist")
    void controller_US03_false_PersonNotExist() {

        //Arrange


        String email = "crispim@gmail.com";
        String gDenomination = "Sunday Runners";
        String gDescription = "All members from Sunday Runners group";


        //Arrange expected result

        //Expected result
        Denomination denomination = Denomination.createDenomination(gDenomination);
        Description description = Description.createDescription(gDescription);
        DateOfCreation dateOfCreation = DateOfCreation.createDateOfCreation(LocalDate.now());

        GroupDTO personIsAddedToGroup= GroupDTOAssembler.createDTOFromDomainObject(denomination, description, dateOfCreation);

        AddPersonToGroupDTO addPersonToGroupDTO = AddPersonToGroupDTOAssembler.createDataTransferObject_Primitives(email, gDenomination);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)

        Mockito.when(us003AddPersonToGroupService.addPersonToGroup(addPersonToGroupDTO)).thenReturn(personIsAddedToGroup);

        //Act
        US003AddPersonToGroupController us003AddPersonToGroupController = new US003AddPersonToGroupController(us003AddPersonToGroupService);
        GroupDTO result = us003AddPersonToGroupController.addPersonToGroup(email, gDenomination);

        //Assert

        assertEquals(personIsAddedToGroup, result);

    }


    @Test
    @DisplayName("Test Controller_US03- GroupID doesn't exist")
    void controller_US03_false_GroupID() {

        //Arrange


        String email = "alexandre@gmail.com";
        String gDenomination = "Sunday";
        String gDescription = "All members from Sunday Runners group";


        //Arrange expected result

        //Expected result
        Denomination denomination = Denomination.createDenomination(gDenomination);
        Description description = Description.createDescription(gDescription);
        DateOfCreation dateOfCreation = DateOfCreation.createDateOfCreation(LocalDate.now());

        GroupDTO personIsAddedToGroup= GroupDTOAssembler.createDTOFromDomainObject(denomination, description, dateOfCreation);

        AddPersonToGroupDTO addPersonToGroupDTO = AddPersonToGroupDTOAssembler.createDataTransferObject_Primitives(email, gDenomination);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)

        Mockito.when(us003AddPersonToGroupService.addPersonToGroup(addPersonToGroupDTO)).thenReturn(personIsAddedToGroup);

        //Act
        US003AddPersonToGroupController us003AddPersonToGroupController = new US003AddPersonToGroupController(us003AddPersonToGroupService);
        GroupDTO result = us003AddPersonToGroupController.addPersonToGroup(email, gDenomination);

        //Assert

        assertEquals(personIsAddedToGroup, result);

    }



}