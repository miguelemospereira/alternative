package switch2019.project.controllerLayer.unitTests.controllersCLI;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import switch2019.project.applicationLayer.applicationServices.US005CreatePersonCategoryService;
import switch2019.project.applicationLayer.dtos.CreatePersonCategoryDTO;
import switch2019.project.applicationLayer.dtos.PersonDTO;
import switch2019.project.applicationLayer.dtosAssemblers.CreatePersonCategoryDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.PersonDTOAssembler;
import switch2019.project.controllerLayer.controllers.controllersCLI.US005CreatePersonCategoryController;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Birthdate;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Birthplace;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Name;
import switch2019.project.domainLayer.domainEntities.vosShared.Email;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class US005CreatePersonCategoryControllerTest extends AbstractTest {

    @Mock
    private US005CreatePersonCategoryService service;

    // SUCCESS

    @Test
    @DisplayName("test for createCategory() | Success")
    public void whenPersonCategoryIsCreated_MsgSuccess() {

        // Arrange

        // Arrange Person
        final String personEmail = "ilda@gmail.com";
        final String personName = "Fontes";
        final LocalDate personBirthdate = LocalDate.of(1964, 02, 16);
        final String personBirthplace = "Vila Nova de Gaia";

        // Arrange Category
        final String categoryDenomination = "Basket";

        // Expected result
        Email email = Email.createEmail(personEmail);
        Name name = Name.createName(personName);
        Birthdate birthdate = Birthdate.createBirthdate(personBirthdate);
        Birthplace birthplace = Birthplace.createBirthplace(personBirthplace);
        PersonID fatherID = null;
        PersonID motherID = null;

        PersonDTO isCategoryCreatedExpected = PersonDTOAssembler.createDTOFromDomainObject(email, name, birthdate, birthplace, fatherID, motherID);

        CreatePersonCategoryDTO createPersonCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(personEmail, categoryDenomination);

        // Mock the behaviour of the service's createCategory method
        Mockito.when(service.createCategory(createPersonCategoryDTO)).thenReturn(isCategoryCreatedExpected);

        // Controller
        US005CreatePersonCategoryController controller = new US005CreatePersonCategoryController(service);

        // Act
        PersonDTO result = controller.createCategory(personEmail, categoryDenomination);

        // Assert
        assertEquals(isCategoryCreatedExpected, result);
    }

    // CATEGORY_ALREADY_EXIST

    @Test
    @DisplayName("test for createCategory() | Category Already Exists")
    public void whenPersonCategoryIsCreated_thenRetrievedMsgIsCategoryAlreadyExists() {

        //Arrange
        String personEmail = "ilda@gmail.com";
        String categoryDenomination = "Netflix";

        CreatePersonCategoryDTO createPersonCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(personEmail, categoryDenomination);

        // Mock the behaviour of the service's createCategory method
        Mockito.when(service.createCategory(createPersonCategoryDTO)).thenThrow(new InvalidArgumentsBusinessException(US005CreatePersonCategoryService.CATEGORY_ALREADY_EXIST));

        // Controller
        US005CreatePersonCategoryController controller = new US005CreatePersonCategoryController(service);

        // Act
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> controller.createCategory(personEmail, categoryDenomination));

        // Assert
        assertEquals(thrown.getMessage(), US005CreatePersonCategoryService.CATEGORY_ALREADY_EXIST);
    }

    // PERSON_DOES_NOT_EXIST

    @Test
    @DisplayName("test for createCategory() | Person Does Not Exist")
    public void whenPersonCategoryIsCreated_thenRetrievedMsgIsPersonDoesNotExists() {

        // Arrange
        String personEmail = "lebron@gmail.com";
        String categoryDenomination = "Basket";

        CreatePersonCategoryDTO createPersonCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(personEmail, categoryDenomination);

        // Mock the behaviour of the service's createCategory method
        Mockito.when(service.createCategory(createPersonCategoryDTO)).thenThrow(new NotFoundArgumentsBusinessException(US005CreatePersonCategoryService.PERSON_DOES_NOT_EXIST));

        // Controller
        US005CreatePersonCategoryController controller = new US005CreatePersonCategoryController(service);

        // Act
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> controller.createCategory(personEmail, categoryDenomination));

        // Assert
        assertEquals(thrown.getMessage(), US005CreatePersonCategoryService.PERSON_DOES_NOT_EXIST);
    }
}
