package switch2019.project.applicationLayer.applicationServices;

/*
    US005 Como utilizador, quero adicionar uma categoria à minha lista de categorias,
    para depois a poder atribuir a um movimento.
*/

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import switch2019.project.applicationLayer.dtos.CreatePersonCategoryDTO;
import switch2019.project.applicationLayer.dtos.PersonDTO;
import switch2019.project.applicationLayer.dtosAssemblers.CreatePersonCategoryDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.PersonDTOAssembler;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Person;
import switch2019.project.domainLayer.domainEntities.vosShared.CategoryID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.ICategoryRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.IPersonRepository;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class US005CreatePersonCategoryServiceTest extends AbstractTest {

    @Mock
    private IPersonRepository personRepository;
    @Mock
    private ICategoryRepository categoryRepository;

    private US005CreatePersonCategoryService us005CreatePersonCategoryService;

    private Person person;

    private PersonID personID;

    @BeforeEach
    public void init() {

        // Ilda
        String ildaEmail = "ilda@gmail.com";
        String ildaName = "Ilda";
        LocalDate ildaBirthdate = LocalDate.of(1999, 2, 20);
        String ildaBirthplace = "Porto";

        this.person = Person.createPerson(ildaEmail, ildaName, ildaBirthdate, ildaBirthplace);
        this.personID = PersonID.createPersonID(ildaEmail);

        // Categories:
        // Salary / Draw Money / IRS / Food / Water Bill / Netflix

        // Salary
        String salaryDenomination = "Salary";
        CategoryID salaryID = CategoryID.createCategoryID(salaryDenomination, personID);
        person.addCategory(salaryID);

        // Draw Money
        String drawMoneyDenomination = "Draw Money";
        CategoryID drawMoneyID = CategoryID.createCategoryID(drawMoneyDenomination, personID);
        person.addCategory(drawMoneyID);

        // IRS
        String irsDenomination = "IRS";
        CategoryID irsID = CategoryID.createCategoryID(irsDenomination, personID);
        person.addCategory(irsID);

        // Food
        String foodDenomination = "Food";
        CategoryID foodID = CategoryID.createCategoryID(foodDenomination, personID);
        person.addCategory(foodID);

        // Water Bill
        String waterBillDenomination = "Water Bill";
        CategoryID waterBillID = CategoryID.createCategoryID(waterBillDenomination, personID);
        person.addCategory(waterBillID);

        // Netflix
        String netflixDenomination = "Netflix";
        CategoryID netflixID = CategoryID.createCategoryID(netflixDenomination, personID);
        person.addCategory(netflixID);
    }

    // Tests

    @Test
    @DisplayName("Test for createCategory() | Success")
    void createCategory_Success() {

        //Arrange
        String email = "ilda@gmail.com";
        String denomination = "Electricity";

        //To Search
        CategoryID categoryID = CategoryID.createCategoryID(denomination, personID);

        //Returning an Optional<Person> Person
        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(person));

        //Returning False
        Mockito.when(categoryRepository.existsById(categoryID)).thenReturn(false);

        //DTO
        CreatePersonCategoryDTO createPersonCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(email, denomination);

        //Expected PersonDTO
        PersonDTO expectedPersonDTO = PersonDTOAssembler.createDTOFromDomainObject(
                person.getPersonID().getEmail(), person.getName(),
                person.getBirthdate(), person.getBirthplace(),
                person.getFather(), person.getMother());

        //Service
        us005CreatePersonCategoryService = new US005CreatePersonCategoryService(
                personRepository, categoryRepository);

        //Act
        PersonDTO result = us005CreatePersonCategoryService.createCategory(createPersonCategoryDTO);

        //Assert
        assertEquals(expectedPersonDTO, result);
        assertEquals(true, person.checkIfPersonHasCategory(categoryID));
    }

    @Test
    @DisplayName("Test For createCategory() | Fail | Category Already Exists")
    void createCategory_Fail_CategoryAlreadyExists() {

        //Arrange
        String email = "ilda@gmail.com";
        String denomination = "Salary";

        //To Search
        CategoryID categoryID = CategoryID.createCategoryID(denomination, personID);

        //Returning an Optional<Person> Person
        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(person));

        //Returning False
        Mockito.when(categoryRepository.existsById(categoryID)).thenReturn(true);

        //DTO
        CreatePersonCategoryDTO createPersonCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(email, denomination);

        //Service
        us005CreatePersonCategoryService = new US005CreatePersonCategoryService(
                personRepository, categoryRepository);

        //Act
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () ->
                us005CreatePersonCategoryService.createCategory(createPersonCategoryDTO));

        //Assert
        assertEquals(thrown.getMessage(), US005CreatePersonCategoryService.CATEGORY_ALREADY_EXIST);
    }

    @Test
    @DisplayName("Test For createCategory() | Fail | Person Does Not Exist")
    void createCategory_Fail_PersonDoesNotExist() {

        //Arrange
        String email = "lebron@gmail.com";
        String denomination = "Electricity";

        //Returning an Optional<Person> Person
        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(person));

        //DTO
        CreatePersonCategoryDTO createPersonCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(email, denomination);

        //Service
        us005CreatePersonCategoryService = new US005CreatePersonCategoryService(
                personRepository, categoryRepository);

        //Act
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () ->
                us005CreatePersonCategoryService.createCategory(createPersonCategoryDTO));

        //Assert
        assertEquals(thrown.getMessage(), US005CreatePersonCategoryService.PERSON_DOES_NOT_EXIST);
    }
}