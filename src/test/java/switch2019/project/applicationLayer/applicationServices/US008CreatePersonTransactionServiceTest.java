package switch2019.project.applicationLayer.applicationServices;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import switch2019.project.applicationLayer.dtos.CreatePersonTransactionDTO;
import switch2019.project.applicationLayer.dtos.DeletePersonTransactionDTO;
import switch2019.project.applicationLayer.dtos.PersonDTO;
import switch2019.project.applicationLayer.dtos.UpdatePersonTransactionDTO;
import switch2019.project.applicationLayer.dtosAssemblers.CreatePersonTransactionDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.DeletePersonTransactionDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.PersonDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.UpdatePersonTransactionDTOAssembler;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Ledger;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Transaction;
import switch2019.project.domainLayer.domainEntities.aggregates.person.*;
import switch2019.project.domainLayer.domainEntities.vosShared.*;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.IAccountRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.ICategoryRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.ILedgerRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.IPersonRepository;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


class US008CreatePersonTransactionServiceTest extends AbstractTest {

    @Mock
    private IPersonRepository personRepository;
    @Mock
    private ICategoryRepository categoryRepository;
    @Mock
    private IAccountRepository accountRepository;
    @Mock
    private ILedgerRepository ledgerRepository;
    private US008CreatePersonTransactionService us008CreatePersonTransactionService;

    //Update / Delete

    private Person personPaulo;
    private PersonID pauloPersonID;
    private Ledger ledger;
    private LedgerID ledgerID;

    //BeforeEach

    @BeforeEach
    public void init() {

        //Ledger
        ledger = Ledger.createLedger();
        ledgerID = ledger.getLedgerID();

        //Paulo

        //Address
        String portoStreet = "Rua Direita do Viso";
        String portoDoorNumber = "59";
        String portoPostCode = "4250 - 198";
        String portoCity = "Porto";
        String portoCountry = "Portugal";

        Address porto = Address.createAddress(portoStreet, portoDoorNumber, portoPostCode, portoCity, portoCountry);

        //Data
        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdate = LocalDate.of(1993, 03, 15);
        String pauloBirthplace = "Vila Nova de Gaia";

        pauloPersonID = PersonID.createPersonID(pauloEmail);

        personPaulo = Person.createPersonWithoutParents(pauloEmail, pauloName, pauloBirthdate, pauloBirthplace, porto, ledgerID);

        //Category Salary
        String salaryDenomination = "Salary";
        CategoryID salaryID = CategoryID.createCategoryID(salaryDenomination, pauloPersonID);
        personPaulo.addCategory(salaryID);

        //Account Company
        String companyDenomination = "Company";
        String companyDescription = "Company account";
        AccountID companyID = AccountID.createAccountID(companyDenomination, pauloPersonID);
        personPaulo.addAccount(companyID);

        //Account Bank Account
        String bankAccountDenomination = "Bank Account";
        String bankAccountDescription = "Personal bank account";
        AccountID bankAccountID = AccountID.createAccountID(bankAccountDenomination, pauloPersonID);
        personPaulo.addAccount(bankAccountID);

        //Salary January

        String credit = "credit";
        String salaryJanuaryDescription = "January salary";
        double salaryJanuaryAmount = 1500;
        LocalDate salaryJanuaryDate = LocalDate.of(2020, 01, 21);
        Transaction salaryJanuary = Transaction.createTransaction(salaryID, credit, salaryJanuaryDescription, salaryJanuaryAmount, salaryJanuaryDate, companyID, bankAccountID);

        ledger.addTransaction(salaryJanuary);

    }

    //Create

    @Test
    @DisplayName("Test for createTransactionAsPerson() | Success")
    void createTransactionAsPerson_Success() {

        //Arrange

        //Person info
        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdate = LocalDate.of(1993, 03, 15);
        String pauloBirthplace = "Vila Nova de Gaia";

        //Transaction info
        final String denominationCategory = "Salary";
        final String type = "debit";
        final String description = "May Salary";
        final double amount = 1500.00;
        final String denominationAccountDeb = "Company";
        final String denominationAccountCred = "Bank Account";
        final String date = "2020-05-26";

        //To Search
        CategoryID categoryID = CategoryID.createCategoryID(denominationCategory, pauloPersonID);
        AccountID debAccountID = AccountID.createAccountID(denominationAccountDeb, pauloPersonID);
        AccountID credAccountID = AccountID.createAccountID(denominationAccountCred, pauloPersonID);

        //Returning an Optional<Person> Paulo Fontes
        Mockito.when(personRepository.findById(pauloPersonID)).thenReturn(Optional.of(personPaulo));

        //Returning True (Category exist)
        Mockito.when(categoryRepository.existsById(categoryID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(debAccountID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(credAccountID)).thenReturn(true);

        //Returning an Optional<Ledger> Paulo Fontes Ledger
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(ledger));

        //DTO
        CreatePersonTransactionDTO createPersonTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(pauloEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date);

        //Expected PersonDTO
        PersonDTO expectedPersonDTO = PersonDTOAssembler.createDTOFromDomainObject(Email.createEmail(pauloEmail), ledgerID, Name.createName(pauloName), Birthdate.createBirthdate(pauloBirthdate), Birthplace.createBirthplace(pauloBirthplace), null, null);

        //Service
        us008CreatePersonTransactionService = new US008CreatePersonTransactionService(personRepository, accountRepository, ledgerRepository, categoryRepository);

        //Act
        PersonDTO result = us008CreatePersonTransactionService.createTransactionAsPerson(createPersonTransactionDTO);

        //Assert
        assertEquals(expectedPersonDTO, result);

    }

    @Test
    @DisplayName("Test for createTransactionAsPerson() | Fail | Person Does Not Exist")
    void createTransactionAsPerson_Fail_PersonDoesNotExist() {

        //Arrange

        //Person info
        String pauloEmail = "paulo@gmail.com";

        //Transaction info
        final String denominationCategory = "Salary";
        final String type = "debit";
        final String description = "May Salary";
        final double amount = 1500.00;
        final String denominationAccountDeb = "Company";
        final String denominationAccountCred = "Bank Account";
        final String date = "2020-05-26";

        //To Search
        CategoryID categoryID = CategoryID.createCategoryID(denominationCategory, pauloPersonID);
        AccountID debAccountID = AccountID.createAccountID(denominationAccountDeb, pauloPersonID);
        AccountID credAccountID = AccountID.createAccountID(denominationAccountCred, pauloPersonID);

        //Returning an Optional<Person> Paulo Fontes
        Mockito.when(personRepository.findById(pauloPersonID)).thenReturn(Optional.empty());

        //Returning True (Category exist)
        Mockito.when(categoryRepository.existsById(categoryID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(debAccountID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(credAccountID)).thenReturn(true);

        //Returning an Optional<Ledger> Paulo Fontes Ledger
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(ledger));

        //DTO
        CreatePersonTransactionDTO createPersonTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(pauloEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date);

        //Service
        us008CreatePersonTransactionService = new US008CreatePersonTransactionService(personRepository, accountRepository, ledgerRepository, categoryRepository);

        //Act
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us008CreatePersonTransactionService.createTransactionAsPerson(createPersonTransactionDTO));

        //Assert
        assertEquals(thrown.getMessage(), US008CreatePersonTransactionService.PERSON_DOES_NOT_EXIST);

    }

    @Test
    @DisplayName("Test for createTransactionAsPerson() | Fail | Category Does Not Exist")
    void createTransactionAsPerson_Fail_CategoryDoesNotExist() {

        //Arrange

        //Person info
        String pauloEmail = "paulo@gmail.com";

        //Transaction info
        final String denominationCategory = "Salary";
        final String type = "debit";
        final String description = "May Salary";
        final double amount = 1500.00;
        final String denominationAccountDeb = "Company";
        final String denominationAccountCred = "Bank Account";
        final String date = "2020-05-26";

        //To Search
        CategoryID categoryID = CategoryID.createCategoryID(denominationCategory, pauloPersonID);
        AccountID debAccountID = AccountID.createAccountID(denominationAccountDeb, pauloPersonID);
        AccountID credAccountID = AccountID.createAccountID(denominationAccountCred, pauloPersonID);

        //Returning an Optional<Person> Paulo Fontes
        Mockito.when(personRepository.findById(pauloPersonID)).thenReturn(Optional.of(personPaulo));

        //Returning True (Category exist)
        Mockito.when(categoryRepository.existsById(categoryID)).thenReturn(false);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(debAccountID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(credAccountID)).thenReturn(true);

        //Returning an Optional<Ledger> Paulo Fontes Ledger
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(ledger));

        //DTO
        CreatePersonTransactionDTO createPersonTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(pauloEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date);

        //Service
        us008CreatePersonTransactionService = new US008CreatePersonTransactionService(personRepository, accountRepository, ledgerRepository, categoryRepository);

        //Act
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us008CreatePersonTransactionService.createTransactionAsPerson(createPersonTransactionDTO));

        //Assert
        assertEquals(thrown.getMessage(), US008CreatePersonTransactionService.CATEGORY_DOES_NOT_EXIST);

    }

    @Test
    @DisplayName("Test for createTransactionAsPerson() | Fail | Debit Account Does Not Exist")
    void createTransactionAsPerson_Fail_DebitAccountDoesNotExist() {

        //Arrange

        //Person info
        String pauloEmail = "paulo@gmail.com";

        //Transaction info
        final String denominationCategory = "Salary";
        final String type = "debit";
        final String description = "May Salary";
        final double amount = 1500.00;
        final String denominationAccountDeb = "Company";
        final String denominationAccountCred = "Bank Account";
        final String date = "2020-05-26";

        //To Search
        CategoryID categoryID = CategoryID.createCategoryID(denominationCategory, pauloPersonID);
        AccountID debAccountID = AccountID.createAccountID(denominationAccountDeb, pauloPersonID);
        AccountID credAccountID = AccountID.createAccountID(denominationAccountCred, pauloPersonID);

        //Returning an Optional<Person> Paulo Fontes
        Mockito.when(personRepository.findById(pauloPersonID)).thenReturn(Optional.of(personPaulo));

        //Returning True (Category exist)
        Mockito.when(categoryRepository.existsById(categoryID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(debAccountID)).thenReturn(false);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(credAccountID)).thenReturn(true);

        //Returning an Optional<Ledger> Paulo Fontes Ledger
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(ledger));

        //DTO
        CreatePersonTransactionDTO createPersonTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(pauloEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date);

        //Service
        us008CreatePersonTransactionService = new US008CreatePersonTransactionService(personRepository, accountRepository, ledgerRepository, categoryRepository);

        //Act
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us008CreatePersonTransactionService.createTransactionAsPerson(createPersonTransactionDTO));

        //Assert
        assertEquals(thrown.getMessage(), US008CreatePersonTransactionService.ACCOUNT_DEB_DOES_NOT_EXIST);

    }

    @Test
    @DisplayName("Test for createTransactionAsPerson() | Fail | Credit Account Does Not Exist")
    void createTransactionAsPerson_Fail_CreditAccountDoesNotExist() {

        //Arrange

        //Person info
        String pauloEmail = "paulo@gmail.com";

        //Transaction info
        final String denominationCategory = "Salary";
        final String type = "debit";
        final String description = "May Salary";
        final double amount = 1500.00;
        final String denominationAccountDeb = "Company";
        final String denominationAccountCred = "Bank Account";
        final String date = "2020-05-26";

        //To Search
        CategoryID categoryID = CategoryID.createCategoryID(denominationCategory, pauloPersonID);
        AccountID debAccountID = AccountID.createAccountID(denominationAccountDeb, pauloPersonID);
        AccountID credAccountID = AccountID.createAccountID(denominationAccountCred, pauloPersonID);

        //Returning an Optional<Person> Paulo Fontes
        Mockito.when(personRepository.findById(pauloPersonID)).thenReturn(Optional.of(personPaulo));

        //Returning True (Category exist)
        Mockito.when(categoryRepository.existsById(categoryID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(debAccountID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(credAccountID)).thenReturn(false);

        //Returning an Optional<Ledger> Paulo Fontes Ledger
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(ledger));

        //DTO
        CreatePersonTransactionDTO createPersonTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(pauloEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date);

        //Service
        us008CreatePersonTransactionService = new US008CreatePersonTransactionService(personRepository, accountRepository, ledgerRepository, categoryRepository);

        //Act
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us008CreatePersonTransactionService.createTransactionAsPerson(createPersonTransactionDTO));

        //Assert
        assertEquals(thrown.getMessage(), US008CreatePersonTransactionService.ACCOUNT_CRED_DOES_NOT_EXIST);

    }


    //Update

    @Test
    @DisplayName("Test for updatePersonTransaction() | Success")
    void updatePersonTransaction_Success() {

        //Arrange

        //Person info
        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdate = LocalDate.of(1993, 03, 15);
        String pauloBirthplace = "Vila Nova de Gaia";

        //Transaction info
        final String denominationCategory = "Salary";
        final String type = "debit";
        final String description = "May Salary";
        final double amount = 1500.00;
        final String denominationAccountDeb = "Company";
        final String denominationAccountCred = "Bank Account";

        //To Search
        CategoryID categoryID = CategoryID.createCategoryID(denominationCategory, pauloPersonID);
        AccountID debAccountID = AccountID.createAccountID(denominationAccountDeb, pauloPersonID);
        AccountID credAccountID = AccountID.createAccountID(denominationAccountCred, pauloPersonID);

        //Returning an Optional<Person> Paulo Fontes
        Mockito.when(personRepository.findById(pauloPersonID)).thenReturn(Optional.of(personPaulo));

        //Returning True (Category exist)
        Mockito.when(categoryRepository.existsById(categoryID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(debAccountID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(credAccountID)).thenReturn(true);

        //Returning an Optional<Ledger> Paulo Fontes Ledger
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(ledger));

        //DTO
        UpdatePersonTransactionDTO updatePersonTransactionDTO = UpdatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(1, pauloEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred);

        //Expected PersonDTO
        PersonDTO expectedPersonDTO = PersonDTOAssembler.createDTOFromDomainObject(Email.createEmail(pauloEmail), ledgerID, Name.createName(pauloName), Birthdate.createBirthdate(pauloBirthdate), Birthplace.createBirthplace(pauloBirthplace), null, null);

        //Service
        us008CreatePersonTransactionService = new US008CreatePersonTransactionService(personRepository, accountRepository, ledgerRepository, categoryRepository);

        //Act
        PersonDTO result = us008CreatePersonTransactionService.updatePersonTransaction(updatePersonTransactionDTO);

        //Assert
        assertEquals(expectedPersonDTO, result);

    }

    @Test
    @DisplayName("Test for updatePersonTransaction() | Fail | Person Does Not Exist")
    void updatePersonTransaction_Fail_PersonDoesNotExist() {

        //Arrange

        //Person info
        String pauloEmail = "paulo@gmail.com";

        //Transaction info
        final String denominationCategory = "Salary";
        final String type = "debit";
        final String description = "May Salary";
        final double amount = 1500.00;
        final String denominationAccountDeb = "Company";
        final String denominationAccountCred = "Bank Account";

        //To Search
        CategoryID categoryID = CategoryID.createCategoryID(denominationCategory, pauloPersonID);
        AccountID debAccountID = AccountID.createAccountID(denominationAccountDeb, pauloPersonID);
        AccountID credAccountID = AccountID.createAccountID(denominationAccountCred, pauloPersonID);

        //Returning an Optional<Person> Paulo Fontes
        Mockito.when(personRepository.findById(pauloPersonID)).thenReturn(Optional.empty());

        //Returning True (Category exist)
        Mockito.when(categoryRepository.existsById(categoryID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(debAccountID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(credAccountID)).thenReturn(true);

        //Returning an Optional<Ledger> Paulo Fontes Ledger
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(ledger));

        //DTO
        UpdatePersonTransactionDTO updatePersonTransactionDTO = UpdatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(1, pauloEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred);

        //Service
        us008CreatePersonTransactionService = new US008CreatePersonTransactionService(personRepository, accountRepository, ledgerRepository, categoryRepository);

        //Act
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us008CreatePersonTransactionService.updatePersonTransaction(updatePersonTransactionDTO));

        //Assert
        assertEquals(thrown.getMessage(), US008CreatePersonTransactionService.PERSON_DOES_NOT_EXIST);

    }

    @Test
    @DisplayName("Test for updatePersonTransaction() | Fail | Category Does Not Exist")
    void updatePersonTransaction_Fail_CategoryDoesNotExist() {

        //Arrange

        //Person info
        String pauloEmail = "paulo@gmail.com";

        //Transaction info
        final String denominationCategory = "Salary";
        final String type = "debit";
        final String description = "May Salary";
        final double amount = 1500.00;
        final String denominationAccountDeb = "Company";
        final String denominationAccountCred = "Bank Account";

        //To Search
        CategoryID categoryID = CategoryID.createCategoryID(denominationCategory, pauloPersonID);
        AccountID debAccountID = AccountID.createAccountID(denominationAccountDeb, pauloPersonID);
        AccountID credAccountID = AccountID.createAccountID(denominationAccountCred, pauloPersonID);

        //Returning an Optional<Person> Paulo Fontes
        Mockito.when(personRepository.findById(pauloPersonID)).thenReturn(Optional.of(personPaulo));

        //Returning True (Category exist)
        Mockito.when(categoryRepository.existsById(categoryID)).thenReturn(false);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(debAccountID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(credAccountID)).thenReturn(true);

        //Returning an Optional<Ledger> Paulo Fontes Ledger
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(ledger));

        //DTO
        UpdatePersonTransactionDTO updatePersonTransactionDTO = UpdatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(1, pauloEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred);

        //Service
        us008CreatePersonTransactionService = new US008CreatePersonTransactionService(personRepository, accountRepository, ledgerRepository, categoryRepository);

        //Act
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us008CreatePersonTransactionService.updatePersonTransaction(updatePersonTransactionDTO));

        //Assert
        assertEquals(thrown.getMessage(), US008CreatePersonTransactionService.CATEGORY_DOES_NOT_EXIST);

    }

    @Test
    @DisplayName("Test for updatePersonTransaction() | Fail | Debit Account Does Not Exist")
    void updatePersonTransaction_Fail_DebitAccountDoesNotExist() {

        //Arrange

        //Person info
        String pauloEmail = "paulo@gmail.com";

        //Transaction info
        final String denominationCategory = "Salary";
        final String type = "debit";
        final String description = "May Salary";
        final double amount = 1500.00;
        final String denominationAccountDeb = "Company";
        final String denominationAccountCred = "Bank Account";

        //To Search
        CategoryID categoryID = CategoryID.createCategoryID(denominationCategory, pauloPersonID);
        AccountID debAccountID = AccountID.createAccountID(denominationAccountDeb, pauloPersonID);
        AccountID credAccountID = AccountID.createAccountID(denominationAccountCred, pauloPersonID);

        //Returning an Optional<Person> Paulo Fontes
        Mockito.when(personRepository.findById(pauloPersonID)).thenReturn(Optional.of(personPaulo));

        //Returning True (Category exist)
        Mockito.when(categoryRepository.existsById(categoryID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(debAccountID)).thenReturn(false);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(credAccountID)).thenReturn(true);

        //Returning an Optional<Ledger> Paulo Fontes Ledger
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(ledger));

        //DTO
        UpdatePersonTransactionDTO updatePersonTransactionDTO = UpdatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(1, pauloEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred);

        //Service
        us008CreatePersonTransactionService = new US008CreatePersonTransactionService(personRepository, accountRepository, ledgerRepository, categoryRepository);

        //Act
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us008CreatePersonTransactionService.updatePersonTransaction(updatePersonTransactionDTO));

        //Assert
        assertEquals(thrown.getMessage(), US008CreatePersonTransactionService.ACCOUNT_DEB_DOES_NOT_EXIST);

    }

    @Test
    @DisplayName("Test for updatePersonTransaction() | Fail | Credit Account Does Not Exist")
    void updatePersonTransaction_Fail_CreditAccountDoesNotExist() {

        //Arrange

        //Person info
        String pauloEmail = "paulo@gmail.com";

        //Transaction info
        final String denominationCategory = "Salary";
        final String type = "debit";
        final String description = "May Salary";
        final double amount = 1500.00;
        final String denominationAccountDeb = "Company";
        final String denominationAccountCred = "Bank Account";

        //To Search
        CategoryID categoryID = CategoryID.createCategoryID(denominationCategory, pauloPersonID);
        AccountID debAccountID = AccountID.createAccountID(denominationAccountDeb, pauloPersonID);
        AccountID credAccountID = AccountID.createAccountID(denominationAccountCred, pauloPersonID);

        //Returning an Optional<Person> Paulo Fontes
        Mockito.when(personRepository.findById(pauloPersonID)).thenReturn(Optional.of(personPaulo));

        //Returning True (Category exist)
        Mockito.when(categoryRepository.existsById(categoryID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(debAccountID)).thenReturn(true);

        //Returning True (Account exist)
        Mockito.when(accountRepository.existsById(credAccountID)).thenReturn(false);

        //Returning an Optional<Ledger> Paulo Fontes Ledger
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(ledger));

        //DTO
        UpdatePersonTransactionDTO updatePersonTransactionDTO = UpdatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(1, pauloEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred);

        //Service
        us008CreatePersonTransactionService = new US008CreatePersonTransactionService(personRepository, accountRepository, ledgerRepository, categoryRepository);

        //Act
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us008CreatePersonTransactionService.updatePersonTransaction(updatePersonTransactionDTO));

        //Assert
        assertEquals(thrown.getMessage(), US008CreatePersonTransactionService.ACCOUNT_CRED_DOES_NOT_EXIST);

    }


    //Delete

    @Test
    @DisplayName("Test for deletePersonTransaction() | Success")
    void deletePersonTransaction_Success() {

        //Arrange

        //Person info
        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdate = LocalDate.of(1993, 03, 15);
        String pauloBirthplace = "Vila Nova de Gaia";

        //Returning an Optional<Person> Paulo Fontes
        Mockito.when(personRepository.findById(pauloPersonID)).thenReturn(Optional.of(personPaulo));

        //Returning an Optional<Ledger> Paulo Fontes Ledger
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(ledger));

        //DTO
        DeletePersonTransactionDTO deletePersonTransactionDTO = DeletePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(1, pauloEmail);

        //Expected PersonDTO
        PersonDTO expectedPersonDTO = PersonDTOAssembler.createDTOFromDomainObject(Email.createEmail(pauloEmail), ledgerID, Name.createName(pauloName), Birthdate.createBirthdate(pauloBirthdate), Birthplace.createBirthplace(pauloBirthplace), null, null);

        //Service
        us008CreatePersonTransactionService = new US008CreatePersonTransactionService(personRepository, accountRepository, ledgerRepository, categoryRepository);

        //Act
        PersonDTO result = us008CreatePersonTransactionService.deletePersonTransaction(deletePersonTransactionDTO);

        //Assert
        assertEquals(expectedPersonDTO, result);

    }

    @Test
    @DisplayName("Test for deletePersonTransaction() | Fail | Person Does Not Exist")
    void deletePersonTransaction_Fail_PersonDoesNotExist() {

        //Arrange

        //Person info
        String pauloEmail = "paulo@gmail.com";

        //Returning an Optional<Person> Paulo Fontes
        Mockito.when(personRepository.findById(pauloPersonID)).thenReturn(Optional.empty());

        //Returning an Optional<Ledger> Paulo Fontes Ledger
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(ledger));

        //DTO
        DeletePersonTransactionDTO deletePersonTransactionDTO = DeletePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(1, pauloEmail);

        //Service
        us008CreatePersonTransactionService = new US008CreatePersonTransactionService(personRepository, accountRepository, ledgerRepository, categoryRepository);

        //Act
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us008CreatePersonTransactionService.deletePersonTransaction(deletePersonTransactionDTO));

        //Assert
        assertEquals(thrown.getMessage(), US008CreatePersonTransactionService.PERSON_DOES_NOT_EXIST);

    }

}