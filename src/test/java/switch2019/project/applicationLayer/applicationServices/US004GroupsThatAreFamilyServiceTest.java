package switch2019.project.applicationLayer.applicationServices;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtos.GroupIDDTO;
import switch2019.project.applicationLayer.dtos.GroupMembersDTO;
import switch2019.project.applicationLayer.dtos.GroupsThatAreFamilyDTO;
import switch2019.project.applicationLayer.dtosAssemblers.GroupDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.GroupMembersDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.GroupsThatAreFamilyDTOAssembler;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.group.Group;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Address;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Person;
import switch2019.project.domainLayer.domainEntities.vosShared.LedgerID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.IGroupRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.IPersonRepository;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


/**
 * @author Fernando Silva
 *//*
 * Fernando Silva created on 20/03/2020
 * inside the PACKAGE switch2019.project.Services
 */
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class US004GroupsThatAreFamilyServiceTest extends AbstractTest {

    @Mock
    private IPersonRepository personRepository;
    @Mock
    private IGroupRepository groupRepository;

    private US004GroupsThatAreFamilyService us004GroupsThatAreFamilyService;

    @Test
    @DisplayName("Test Controller_US04 - GroupsThatAreFamily - 1 group")
    void controller_US04_OneGroupFamily() {
//      Instantiating an us004GroupsThatAreFamilyService with groupRepository and personRepository
        us004GroupsThatAreFamilyService = new US004GroupsThatAreFamilyService(groupRepository,personRepository);

//        Arrange address
        String street = "Rua do molhe";
        String doorNumber = "2265";
        String postCode = "4569";
        String city = "Póvoa";
        String country = "Portugal";
        Address address = Address.createAddress(street,doorNumber,postCode,city,country);

//        Arrange person Manuel
        String manuelEmail = "manuel@gmail.com";
        String manuelName = "Manuel Fontes";
        LocalDate manuelBirthdateLD = LocalDate.of(1964, 1, 16);
        String manuelBirthplace = "Vila Nova de Gaia";
        Person manuel = Person.createPerson(manuelEmail,manuelName,manuelBirthdateLD,manuelBirthplace);

//        Arrange person Ilda
        String ildaEmail = "ilda@gmail.com";
        String ildaName = "Ilda Fontes";
        LocalDate ildaBirthdateLD = LocalDate.of(1963, 1, 9);
        String ildaBirthplace = "Vila Nova de Gaia";
        Person ilda = Person.createPerson(ildaEmail,ildaName,ildaBirthdateLD,ildaBirthplace);

//        Arrange person Paulo
        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdateLD = LocalDate.of(1993, 3, 15);
        String pauloBirthplace = "Vila Nova de Gaia";
        LedgerID pauloLedgerID = LedgerID.createLedgerID();
        Person paulo = Person.createPersonWithParents(pauloEmail,pauloName,pauloBirthdateLD,ilda.getPersonID(),manuel.getPersonID(),pauloBirthplace,address,pauloLedgerID);

//        Arrange person Helder
        String helderEmail = "helder@gmail.com";
        String helderName = "Helder Fontes";
        LocalDate helderBirthdateLD = LocalDate.of(1983, 1, 30);
        String helderBirthplace = "Vila Nova de Gaia";
        LedgerID helderLedgerID = LedgerID.createLedgerID();
        Person helder = Person.createPersonWithParents(helderEmail,helderName,helderBirthdateLD,ilda.getPersonID(),manuel.getPersonID(),helderBirthplace,address,helderLedgerID);

//        Add Helder to Paulo list of siblings
        paulo.addSibling(helder.getPersonID());

//        Arrange Group

        String groupDenomination = "Fontes Family";
        LocalDate groupDateOfCreation = LocalDate.of(2020,05,27);
        String groupDescription = "All members from Fontes family";
        LedgerID groupLedgerID = LedgerID.createLedgerID();

        Group fontesFamily = Group.createGroupAsPersonInCharge(groupDenomination,ilda.getPersonID(),groupDescription,groupDateOfCreation,groupLedgerID);
        fontesFamily.addPersonInCharge(manuel.getPersonID());
        fontesFamily.addMember(paulo.getPersonID());
        fontesFamily.addMember(helder.getPersonID());

        List<Group> listOfGroups = new ArrayList<>();
        listOfGroups.add(fontesFamily);

        //        Act
//          Mock the behaviour of groupRepository
//        Returning an Optional<Group> pauloFamily
        Mockito.when(groupRepository.findAll())
                .thenReturn(listOfGroups);

        //          Mock the behaviour of personRepository
//        Returning an Optional<Person> ilda
        Mockito.when(personRepository.findById(ilda.getPersonID()))
                .thenReturn(Optional.of(ilda));

        //          Mock the behaviour of personRepository
//        Returning an Optional<Person> manuel
        Mockito.when(personRepository.findById(manuel.getPersonID()))
                .thenReturn(Optional.of(manuel));

        //          Mock the behaviour of personRepository
//        Returning an Optional<Person> paulo
        Mockito.when(personRepository.findById(paulo.getPersonID()))
                .thenReturn(Optional.of(paulo));

        //          Mock the behaviour of personRepository
//        Returning an Optional<Person> helder
        Mockito.when(personRepository.findById(helder.getPersonID()))
                .thenReturn(Optional.of(helder));

//        Expected GroupsThatAreFamily

        GroupIDDTO groupIDDTOFontesFamily = new GroupIDDTO(fontesFamily.getGroupID().getDenomination().getDenomination());
        List<GroupIDDTO> listOfGroupIDDTO = new ArrayList<>();
        listOfGroupIDDTO.add(groupIDDTOFontesFamily);

        GroupsThatAreFamilyDTO expectedGroupsThatAreFamily = GroupsThatAreFamilyDTOAssembler.createDTOFromDomainObject(listOfGroupIDDTO);

//      Assert

        GroupsThatAreFamilyDTO groupsThatAreFamilyDTO = us004GroupsThatAreFamilyService.groupsThatAreFamily();

        assertEquals(expectedGroupsThatAreFamily,groupsThatAreFamilyDTO);
    }

    @Test
    @DisplayName("Test Controller_US04 - GroupsThatAreFamily - no groups - mother and father null")
    void controller_US04_NoGroupFamily() {
//      Instantiating an us004GroupsThatAreFamilyService with groupRepository and personRepository
        us004GroupsThatAreFamilyService = new US004GroupsThatAreFamilyService(groupRepository,personRepository);

//        Arrange address
        String street = "Rua do molhe";
        String doorNumber = "2265";
        String postCode = "4569";
        String city = "Póvoa";
        String country = "Portugal";
        Address address = Address.createAddress(street,doorNumber,postCode,city,country);

//        Arrange person Paulo
        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdateLD = LocalDate.of(1993, 3, 15);
        String pauloBirthplace = "Vila Nova de Gaia";
        LedgerID pauloLedgerID = LedgerID.createLedgerID();
        Person paulo = Person.createPersonWithoutParents(pauloEmail,pauloName,pauloBirthdateLD,pauloBirthplace,address,pauloLedgerID);

//        Arrange person Helder
        String helderEmail = "helder@gmail.com";
        String helderName = "Helder Fontes";
        LocalDate helderBirthdateLD = LocalDate.of(1983, 1, 30);
        String helderBirthplace = "Vila Nova de Gaia";
        LedgerID helderLedgerID = LedgerID.createLedgerID();
        Person helder = Person.createPersonWithoutParents(helderEmail,helderName,helderBirthdateLD,helderBirthplace,address,helderLedgerID);

//        Add Helder to Paulo list of siblings
        paulo.addSibling(helder.getPersonID());

//        Arrange Group

        String groupDenomination = "Fontes Group";
        LocalDate groupDateOfCreation = LocalDate.of(2020,05,27);
        String groupDescription = "All members from Fontes Group";
        LedgerID groupLedgerID = LedgerID.createLedgerID();

        Group fontesGroup = Group.createGroupAsPersonInCharge(groupDenomination,paulo.getPersonID(),groupDescription,groupDateOfCreation,groupLedgerID);
        fontesGroup.addMember(helder.getPersonID());

        List<Group> listOfGroups = new ArrayList<>();
        listOfGroups.add(fontesGroup);

        //        Act
//          Mock the behaviour of groupRepository
//        Returning an Optional<Group> pauloFamily
        Mockito.when(groupRepository.findAll())
                .thenReturn(listOfGroups);

        //          Mock the behaviour of personRepository
//        Returning an Optional<Person> paulo
        Mockito.when(personRepository.findById(paulo.getPersonID()))
                .thenReturn(Optional.of(paulo));

        //          Mock the behaviour of personRepository
//        Returning an Optional<Person> helder
        Mockito.when(personRepository.findById(helder.getPersonID()))
                .thenReturn(Optional.of(helder));

//        Expected GroupsThatAreFamily

        List<GroupIDDTO> listOfGroupIDDTO = new ArrayList<>();

        GroupsThatAreFamilyDTO expectedGroupsThatAreFamily = GroupsThatAreFamilyDTOAssembler.createDTOFromDomainObject(listOfGroupIDDTO);

//      Assert

        GroupsThatAreFamilyDTO groupsThatAreFamilyDTO = us004GroupsThatAreFamilyService.groupsThatAreFamily();

        assertEquals(expectedGroupsThatAreFamily,groupsThatAreFamilyDTO);
    }

    @Test
    @DisplayName("Test Controller_US04 - GroupsThatAreFamily - 0 group")
    void controller_US04_NoGroupFamilyDifferentList() {
//      Instantiating an us004GroupsThatAreFamilyService with groupRepository and personRepository
        us004GroupsThatAreFamilyService = new US004GroupsThatAreFamilyService(groupRepository,personRepository);

//        Arrange address
        String street = "Rua do molhe";
        String doorNumber = "2265";
        String postCode = "4569";
        String city = "Póvoa";
        String country = "Portugal";
        Address address = Address.createAddress(street,doorNumber,postCode,city,country);

//        Arrange person Manuel
        String manuelEmail = "manuel@gmail.com";
        String manuelName = "Manuel Fontes";
        LocalDate manuelBirthdateLD = LocalDate.of(1964, 1, 16);
        String manuelBirthplace = "Vila Nova de Gaia";
        Person manuel = Person.createPerson(manuelEmail,manuelName,manuelBirthdateLD,manuelBirthplace);

//        Arrange person Ilda
        String ildaEmail = "ilda@gmail.com";
        String ildaName = "Ilda Fontes";
        LocalDate ildaBirthdateLD = LocalDate.of(1963, 1, 9);
        String ildaBirthplace = "Vila Nova de Gaia";
        Person ilda = Person.createPerson(ildaEmail,ildaName,ildaBirthdateLD,ildaBirthplace);

//        Arrange person Paulo
        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdateLD = LocalDate.of(1993, 3, 15);
        String pauloBirthplace = "Vila Nova de Gaia";
        LedgerID pauloLedgerID = LedgerID.createLedgerID();
        Person paulo = Person.createPersonWithParents(pauloEmail,pauloName,pauloBirthdateLD,ilda.getPersonID(),manuel.getPersonID(),pauloBirthplace,address,pauloLedgerID);

//        Arrange person Helder
        String helderEmail = "helder@gmail.com";
        String helderName = "Helder Fontes";
        LocalDate helderBirthdateLD = LocalDate.of(1983, 1, 30);
        String helderBirthplace = "Vila Nova de Gaia";
        LedgerID helderLedgerID = LedgerID.createLedgerID();
        Person helder = Person.createPersonWithParents(helderEmail,helderName,helderBirthdateLD,ilda.getPersonID(),manuel.getPersonID(),helderBirthplace,address,helderLedgerID);

//        Arrange person Joana
        String joanaEmail = "joana@gmail.com";
        String joanaName = "Joana Silva";
        LocalDate joanaBirthdateLD = LocalDate.of(1983, 1, 30);
        String joanaBirthplace = "Vila Nova de Gaia";
        LedgerID joanaLedgerID = LedgerID.createLedgerID();
        Person joana = Person.createPersonWithoutParents(joanaEmail,joanaName,joanaBirthdateLD,joanaBirthplace,address,joanaLedgerID);

        //        Add Helder to Paulo list of siblings
        paulo.addSibling(helder.getPersonID());

//        Arrange Group

        String groupDenomination = "Fontes Friends";
        LocalDate groupDateOfCreation = LocalDate.of(2020,05,27);
        String groupDescription = "All members from Fontes friends";
        LedgerID groupLedgerID = LedgerID.createLedgerID();

        Group fontesFriends = Group.createGroupAsPersonInCharge(groupDenomination,ilda.getPersonID(),groupDescription,groupDateOfCreation,groupLedgerID);
        fontesFriends.addPersonInCharge(manuel.getPersonID());
        fontesFriends.addMember(paulo.getPersonID());
        fontesFriends.addMember(helder.getPersonID());
        fontesFriends.addMember(joana.getPersonID());

        List<Group> listOfGroups = new ArrayList<>();
        listOfGroups.add(fontesFriends);

        //        Act
//          Mock the behaviour of groupRepository
//        Returning an Optional<Group> pauloFamily
        Mockito.when(groupRepository.findAll())
                .thenReturn(listOfGroups);

        //          Mock the behaviour of personRepository
//        Returning an Optional<Person> ilda
        Mockito.when(personRepository.findById(ilda.getPersonID()))
                .thenReturn(Optional.of(ilda));

        //          Mock the behaviour of personRepository
//        Returning an Optional<Person> manuel
        Mockito.when(personRepository.findById(manuel.getPersonID()))
                .thenReturn(Optional.of(manuel));

        //          Mock the behaviour of personRepository
//        Returning an Optional<Person> paulo
        Mockito.when(personRepository.findById(paulo.getPersonID()))
                .thenReturn(Optional.of(paulo));

        //          Mock the behaviour of personRepository
//        Returning an Optional<Person> helder
        Mockito.when(personRepository.findById(helder.getPersonID()))
                .thenReturn(Optional.of(helder));

        //          Mock the behaviour of personRepository
//        Returning an Optional<Person> helder
        Mockito.when(personRepository.findById(joana.getPersonID()))
                .thenReturn(Optional.of(joana));

//        Expected GroupsThatAreFamily

        List<GroupIDDTO> listOfGroupIDDTO = new ArrayList<>();

        GroupsThatAreFamilyDTO expectedGroupsThatAreFamily = GroupsThatAreFamilyDTOAssembler.createDTOFromDomainObject(listOfGroupIDDTO);

//      Assert

        GroupsThatAreFamilyDTO groupsThatAreFamilyDTO = us004GroupsThatAreFamilyService.groupsThatAreFamily();

        assertEquals(expectedGroupsThatAreFamily,groupsThatAreFamilyDTO);
    }

    @Test
    @DisplayName("Test Controller_US04 - GroupsThatAreFamily - getDenomination")
    void controller_US04_getDenomination() {
//      Instantiating an us004GroupsThatAreFamilyService with groupRepository and personRepository
        us004GroupsThatAreFamilyService = new US004GroupsThatAreFamilyService(groupRepository,personRepository);

//        Arrange address
        String street = "Rua do molhe";
        String doorNumber = "2265";
        String postCode = "4569";
        String city = "Póvoa";
        String country = "Portugal";
        Address address = Address.createAddress(street,doorNumber,postCode,city,country);

//        Arrange person Manuel
        String manuelEmail = "manuel@gmail.com";
        String manuelName = "Manuel Fontes";
        LocalDate manuelBirthdateLD = LocalDate.of(1964, 1, 16);
        String manuelBirthplace = "Vila Nova de Gaia";
        Person manuel = Person.createPerson(manuelEmail,manuelName,manuelBirthdateLD,manuelBirthplace);

//        Arrange person Ilda
        String ildaEmail = "ilda@gmail.com";
        String ildaName = "Ilda Fontes";
        LocalDate ildaBirthdateLD = LocalDate.of(1963, 1, 9);
        String ildaBirthplace = "Vila Nova de Gaia";
        Person ilda = Person.createPerson(ildaEmail,ildaName,ildaBirthdateLD,ildaBirthplace);

//        Arrange person Paulo
        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdateLD = LocalDate.of(1993, 3, 15);
        String pauloBirthplace = "Vila Nova de Gaia";
        LedgerID pauloLedgerID = LedgerID.createLedgerID();
        Person paulo = Person.createPersonWithParents(pauloEmail,pauloName,pauloBirthdateLD,ilda.getPersonID(),manuel.getPersonID(),pauloBirthplace,address,pauloLedgerID);

//        Arrange person Helder
        String helderEmail = "helder@gmail.com";
        String helderName = "Helder Fontes";
        LocalDate helderBirthdateLD = LocalDate.of(1983, 1, 30);
        String helderBirthplace = "Vila Nova de Gaia";
        LedgerID helderLedgerID = LedgerID.createLedgerID();
        Person helder = Person.createPersonWithParents(helderEmail,helderName,helderBirthdateLD,ilda.getPersonID(),manuel.getPersonID(),helderBirthplace,address,helderLedgerID);

//        Add Helder to Paulo list of siblings
        paulo.addSibling(helder.getPersonID());

//        Arrange Group

        String groupDenomination = "Fontes Family";
        LocalDate groupDateOfCreation = LocalDate.of(2020,05,27);
        String groupDescription = "All members from Fontes family";
        LedgerID groupLedgerID = LedgerID.createLedgerID();

        Group fontesFamily = Group.createGroupAsPersonInCharge(groupDenomination,ilda.getPersonID(),groupDescription,groupDateOfCreation,groupLedgerID);
        fontesFamily.addPersonInCharge(manuel.getPersonID());
        fontesFamily.addMember(paulo.getPersonID());
        fontesFamily.addMember(helder.getPersonID());

        //        Act
//          Mock the behaviour of groupRepository
//        Returning an Optional<Group> pauloFamily
        Mockito.when(groupRepository.findById(fontesFamily.getGroupID()))
                .thenReturn(Optional.of(fontesFamily));

//        Expected GroupsThatAreFamily

        GroupDTO expetedGroupDTO = GroupDTOAssembler.createDTOFromDomainObject(fontesFamily.getGroupID().getDenomination(),fontesFamily.getDescription(),fontesFamily.getDateOfCreation());

//      Assert

        String fontesFamilyDenomination = fontesFamily.getGroupID().getDenomination().getDenomination();

        GroupDTO groupDTO = us004GroupsThatAreFamilyService.getGroupByDenomination(fontesFamilyDenomination);

        assertEquals(expetedGroupDTO,groupDTO);
    }

    @Test
    @DisplayName("Test Controller_US04 - GroupsThatAreFamily - getDenomination - Group doesnt exist")
    void controller_US04_getDenomination_GroupNotExist() {
//      Instantiating an us004GroupsThatAreFamilyService with groupRepository and personRepository
        us004GroupsThatAreFamilyService = new US004GroupsThatAreFamilyService(groupRepository,personRepository);

//        Arrange address
        String street = "Rua do molhe";
        String doorNumber = "2265";
        String postCode = "4569";
        String city = "Póvoa";
        String country = "Portugal";
        Address address = Address.createAddress(street,doorNumber,postCode,city,country);

//        Arrange person Manuel
        String manuelEmail = "manuel@gmail.com";
        String manuelName = "Manuel Fontes";
        LocalDate manuelBirthdateLD = LocalDate.of(1964, 1, 16);
        String manuelBirthplace = "Vila Nova de Gaia";
        Person manuel = Person.createPerson(manuelEmail,manuelName,manuelBirthdateLD,manuelBirthplace);

//        Arrange person Ilda
        String ildaEmail = "ilda@gmail.com";
        String ildaName = "Ilda Fontes";
        LocalDate ildaBirthdateLD = LocalDate.of(1963, 1, 9);
        String ildaBirthplace = "Vila Nova de Gaia";
        Person ilda = Person.createPerson(ildaEmail,ildaName,ildaBirthdateLD,ildaBirthplace);

//        Arrange person Paulo
        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdateLD = LocalDate.of(1993, 3, 15);
        String pauloBirthplace = "Vila Nova de Gaia";
        LedgerID pauloLedgerID = LedgerID.createLedgerID();
        Person paulo = Person.createPersonWithParents(pauloEmail,pauloName,pauloBirthdateLD,ilda.getPersonID(),manuel.getPersonID(),pauloBirthplace,address,pauloLedgerID);

//        Arrange person Helder
        String helderEmail = "helder@gmail.com";
        String helderName = "Helder Fontes";
        LocalDate helderBirthdateLD = LocalDate.of(1983, 1, 30);
        String helderBirthplace = "Vila Nova de Gaia";
        LedgerID helderLedgerID = LedgerID.createLedgerID();
        Person helder = Person.createPersonWithParents(helderEmail,helderName,helderBirthdateLD,ilda.getPersonID(),manuel.getPersonID(),helderBirthplace,address,helderLedgerID);

//        Add Helder to Paulo list of siblings
        paulo.addSibling(helder.getPersonID());

//        Arrange Group

        String groupDenomination = "Fontes Family";
        LocalDate groupDateOfCreation = LocalDate.of(2020,05,27);
        String groupDescription = "All members from Fontes family";
        LedgerID groupLedgerID = LedgerID.createLedgerID();

        Group fontesFamily = Group.createGroupAsPersonInCharge(groupDenomination,ilda.getPersonID(),groupDescription,groupDateOfCreation,groupLedgerID);
        fontesFamily.addPersonInCharge(manuel.getPersonID());
        fontesFamily.addMember(paulo.getPersonID());
        fontesFamily.addMember(helder.getPersonID());

        //        Act
//          Mock the behaviour of groupRepository
//        Returning an Optional<Group> pauloFamily
        Mockito.when(groupRepository.findById(fontesFamily.getGroupID()))
                .thenReturn(Optional.empty());

        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us004GroupsThatAreFamilyService.getGroupByDenomination(fontesFamily.getGroupID().getDenomination().getDenomination()));

        //Assert
        assertEquals(thrown.getMessage(), US004GroupsThatAreFamilyService.GROUP_DOES_NOT_EXIST);
    }

    @Test
    @DisplayName("Test Controller_US04 - GroupsThatAreFamily - getAllMembers")
    void controller_US04_getAllMembers() {
//      Instantiating an us004GroupsThatAreFamilyService with groupRepository and personRepository
        us004GroupsThatAreFamilyService = new US004GroupsThatAreFamilyService(groupRepository,personRepository);

//        Arrange address
        String street = "Rua do molhe";
        String doorNumber = "2265";
        String postCode = "4569";
        String city = "Póvoa";
        String country = "Portugal";
        Address address = Address.createAddress(street,doorNumber,postCode,city,country);

//        Arrange person Manuel
        String manuelEmail = "manuel@gmail.com";
        String manuelName = "Manuel Fontes";
        LocalDate manuelBirthdateLD = LocalDate.of(1964, 1, 16);
        String manuelBirthplace = "Vila Nova de Gaia";
        Person manuel = Person.createPerson(manuelEmail,manuelName,manuelBirthdateLD,manuelBirthplace);

//        Arrange person Ilda
        String ildaEmail = "ilda@gmail.com";
        String ildaName = "Ilda Fontes";
        LocalDate ildaBirthdateLD = LocalDate.of(1963, 1, 9);
        String ildaBirthplace = "Vila Nova de Gaia";
        Person ilda = Person.createPerson(ildaEmail,ildaName,ildaBirthdateLD,ildaBirthplace);

//        Arrange person Paulo
        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdateLD = LocalDate.of(1993, 3, 15);
        String pauloBirthplace = "Vila Nova de Gaia";
        LedgerID pauloLedgerID = LedgerID.createLedgerID();
        Person paulo = Person.createPersonWithParents(pauloEmail,pauloName,pauloBirthdateLD,ilda.getPersonID(),manuel.getPersonID(),pauloBirthplace,address,pauloLedgerID);

//        Arrange person Helder
        String helderEmail = "helder@gmail.com";
        String helderName = "Helder Fontes";
        LocalDate helderBirthdateLD = LocalDate.of(1983, 1, 30);
        String helderBirthplace = "Vila Nova de Gaia";
        LedgerID helderLedgerID = LedgerID.createLedgerID();
        Person helder = Person.createPersonWithParents(helderEmail,helderName,helderBirthdateLD,ilda.getPersonID(),manuel.getPersonID(),helderBirthplace,address,helderLedgerID);

//        Add Helder to Paulo list of siblings
        paulo.addSibling(helder.getPersonID());

//        Arrange Group

        String groupDenomination = "Fontes Family";
        LocalDate groupDateOfCreation = LocalDate.of(2020,05,27);
        String groupDescription = "All members from Fontes family";
        LedgerID groupLedgerID = LedgerID.createLedgerID();

        Group fontesFamily = Group.createGroupAsPersonInCharge(groupDenomination,ilda.getPersonID(),groupDescription,groupDateOfCreation,groupLedgerID);
        fontesFamily.addPersonInCharge(manuel.getPersonID());
        fontesFamily.addMember(paulo.getPersonID());
        fontesFamily.addMember(helder.getPersonID());

        //        Act
//          Mock the behaviour of groupRepository
//        Returning an Optional<Group> pauloFamily
        Mockito.when(groupRepository.findById(fontesFamily.getGroupID()))
                .thenReturn(Optional.of(fontesFamily));

//        Expected GroupsMembersDTO

        List<PersonID> listOfPersonsID = new ArrayList<>();
        listOfPersonsID.add(ilda.getPersonID());
        listOfPersonsID.add(manuel.getPersonID());
        listOfPersonsID.add(paulo.getPersonID());
        listOfPersonsID.add(helder.getPersonID());

        GroupMembersDTO expectedGroupMembersDTO = GroupMembersDTOAssembler.createDTOFromDomainObject(listOfPersonsID);

//      Assert

        String fontesFamilyDenomination = fontesFamily.getGroupID().getDenomination().getDenomination();

        GroupMembersDTO groupMembersDTO = us004GroupsThatAreFamilyService.getGroupAllMembers(fontesFamilyDenomination);

        assertEquals(expectedGroupMembersDTO,groupMembersDTO);
    }

    @Test
    @DisplayName("Test Controller_US04 - GroupsThatAreFamily - getAllMembers - Group doesnt exist")
    void controller_US04_getAllMembers_GroupNotExist() {
//      Instantiating an us004GroupsThatAreFamilyService with groupRepository and personRepository
        us004GroupsThatAreFamilyService = new US004GroupsThatAreFamilyService(groupRepository,personRepository);

//        Arrange address
        String street = "Rua do molhe";
        String doorNumber = "2265";
        String postCode = "4569";
        String city = "Póvoa";
        String country = "Portugal";
        Address address = Address.createAddress(street,doorNumber,postCode,city,country);

//        Arrange person Manuel
        String manuelEmail = "manuel@gmail.com";
        String manuelName = "Manuel Fontes";
        LocalDate manuelBirthdateLD = LocalDate.of(1964, 1, 16);
        String manuelBirthplace = "Vila Nova de Gaia";
        Person manuel = Person.createPerson(manuelEmail,manuelName,manuelBirthdateLD,manuelBirthplace);

//        Arrange person Ilda
        String ildaEmail = "ilda@gmail.com";
        String ildaName = "Ilda Fontes";
        LocalDate ildaBirthdateLD = LocalDate.of(1963, 1, 9);
        String ildaBirthplace = "Vila Nova de Gaia";
        Person ilda = Person.createPerson(ildaEmail,ildaName,ildaBirthdateLD,ildaBirthplace);

//        Arrange person Paulo
        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdateLD = LocalDate.of(1993, 3, 15);
        String pauloBirthplace = "Vila Nova de Gaia";
        LedgerID pauloLedgerID = LedgerID.createLedgerID();
        Person paulo = Person.createPersonWithParents(pauloEmail,pauloName,pauloBirthdateLD,ilda.getPersonID(),manuel.getPersonID(),pauloBirthplace,address,pauloLedgerID);

//        Arrange person Helder
        String helderEmail = "helder@gmail.com";
        String helderName = "Helder Fontes";
        LocalDate helderBirthdateLD = LocalDate.of(1983, 1, 30);
        String helderBirthplace = "Vila Nova de Gaia";
        LedgerID helderLedgerID = LedgerID.createLedgerID();
        Person helder = Person.createPersonWithParents(helderEmail,helderName,helderBirthdateLD,ilda.getPersonID(),manuel.getPersonID(),helderBirthplace,address,helderLedgerID);

//        Add Helder to Paulo list of siblings
        paulo.addSibling(helder.getPersonID());

//        Arrange Group

        String groupDenomination = "Fontes Family";
        LocalDate groupDateOfCreation = LocalDate.of(2020,05,27);
        String groupDescription = "All members from Fontes family";
        LedgerID groupLedgerID = LedgerID.createLedgerID();

        Group fontesFamily = Group.createGroupAsPersonInCharge(groupDenomination,ilda.getPersonID(),groupDescription,groupDateOfCreation,groupLedgerID);
        fontesFamily.addPersonInCharge(manuel.getPersonID());
        fontesFamily.addMember(paulo.getPersonID());
        fontesFamily.addMember(helder.getPersonID());

        //        Act
//          Mock the behaviour of groupRepository
//        Returning an Optional<Group> pauloFamily
        Mockito.when(groupRepository.findById(fontesFamily.getGroupID()))
                .thenReturn(Optional.of(fontesFamily));

//        Expected GroupsMembersDTO

        List<PersonID> listOfPersonsID = new ArrayList<>();
        listOfPersonsID.add(ilda.getPersonID());
        listOfPersonsID.add(manuel.getPersonID());
        listOfPersonsID.add(paulo.getPersonID());
        listOfPersonsID.add(helder.getPersonID());

        //        Act
//          Mock the behaviour of groupRepository
//        Returning an Optional<Group> pauloFamily
        Mockito.when(groupRepository.findById(fontesFamily.getGroupID()))
                .thenReturn(Optional.empty());

        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us004GroupsThatAreFamilyService.getGroupAllMembers(fontesFamily.getGroupID().getDenomination().getDenomination()));

        //Assert
        assertEquals(thrown.getMessage(), US004GroupsThatAreFamilyService.GROUP_DOES_NOT_EXIST);
    }



}
