package switch2019.project.applicationLayer.dtosAssemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switch2019.project.applicationLayer.dtos.GroupIDDTO;
import switch2019.project.domainLayer.domainEntities.vosShared.GroupID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GroupIDDTOAssemblerTest {

    @Test
    @DisplayName("Test create Data Transfer Object with Primitives - GroupID || Happy case")
    void testCreateDataTransferObjectWithPrimitives() {

        String denomination = "Dance";
        GroupID groupID = GroupID.createGroupID(denomination);


        GroupIDDTO createGroupIDTOExpected = new GroupIDDTO(denomination);

        GroupIDDTOAssembler groupIDDTOAssembler = new GroupIDDTOAssembler();

        GroupIDDTO groupIDDTO = groupIDDTOAssembler.createDTOFromDomainObject(groupID);

        assertEquals(createGroupIDTOExpected, groupIDDTO);

    }

}